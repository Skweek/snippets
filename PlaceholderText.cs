using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace Snippets
{
    /// <summary>
    /// Logic for the placeholder textbox adorner
    /// </summary>
    public class PlaceholderTextAdorner : Adorner
    {
        private readonly string _text;
        public PlaceholderTextAdorner(UIElement adornedElement, string a_Text) : base(adornedElement)
        {
            IsHitTestVisible = false;
            _text = a_Text;
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            var placeholderText = new FormattedText(_text,
                                  System.Globalization.CultureInfo.GetCultureInfo("en-us"),
                                  FlowDirection.LeftToRight,
                                  new Typeface("Verdana"),
                                  (AdornedElement as TextBox).FontSize,
                                  Brushes.LightGray);
            // Draw the text 5 pixels in and down, might need to be tidied up
            drawingContext.DrawText(placeholderText, new Point(5, 5));
        }
    }

    /// <summary>
    /// Interaction logic for PlaceholderText.xaml
    /// </summary>
    public partial class PlaceholderText : TextBox
    {
        private AdornerLayer _adornerLayer;
        private PlaceholderTextAdorner _adorner;
        private bool _hasAdorner = false;

        [Description("Placeholder Text"), Category("Data")]
        public string Placeholder { get; set; }

        public PlaceholderText()
        {
            InitializeComponent();
        }
        protected override void OnGotFocus(RoutedEventArgs e)
        {
            base.OnGotFocus(e);

            // On focus remove the placeholder text
            if (_hasAdorner)
            {
                _adornerLayer.Remove(_adorner);
                _hasAdorner = false;
            }
        }
        protected override void OnLostFocus(RoutedEventArgs e)
        {
            base.OnLostFocus(e);

            // On lost focus render the placeholder text if needed
            if (Text == "" && Placeholder != null && Placeholder != "")
            {
                if (!_hasAdorner)
                {
                    _adornerLayer.Add(_adorner);
                    _hasAdorner = true;
                }
            }
        }

        protected override void OnTextChanged(TextChangedEventArgs e)
        {
            base.OnTextChanged(e);
            // Also show placeholder text if user has removed all text in control
            if (Text == "" && Placeholder != null && Placeholder != "")
            {
                if (!_hasAdorner)
                {
                    _adornerLayer.Add(_adorner);
                    _hasAdorner = true;
                }
            }
        }

        // We override this as a catch all and to handle creating our objects once the text box has actually been rendered
        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);
            // Create our objects
            if (_adornerLayer == null)
            {
                _adornerLayer = AdornerLayer.GetAdornerLayer(this);
                _adorner = new PlaceholderTextAdorner(this, Placeholder);
            }
            if (Text == "" && Placeholder != null && Placeholder != "")
            {
                if (!_hasAdorner)
                {
                    _adornerLayer.Add(_adorner);
                    _hasAdorner = true;
                }
            }

        }
    }
}
